'''
Created on Oct 6, 2015
the goal of this script to generate process metric  for each file based on git logs are used in software  analysis. here are the metrics list supported:
short Name                      Description
COMM                          Commit Count
ADEV                          Active Dev Count
DDEV                          Distinct Dev Count
ADD                           Normalized Lines Added
DEL                           Normalized Lines Deleted
OWN                           Owners Contributed Lines
MINOR                         Minor Contributor Count
SCTR                          Changed Code Scattering
NADEV                         Neighbors Active Dev Count
NDDEV                         Neighbors Distinct Dev Count
NCOMM                         Neighbors Commit Count
NSCTR                         Neighbors Change Scatttering
OEXP                          Owners Experience
EXP                           All Committers Experience

@author: Mojtaba  Queen University
'''
import re
import sys 
class gitParser:
    logPath=""
    inputLogFormat=""
    outputLogFormat=""
    logsData=[]
    logPointer=None
    filesMetric={}
    releaseAllChanges=0
    devList={}
    def loadLogs(self):
        try:
            self.logPointer=open(self.logPath,'r')
        except IOError as detail:
            print "can't open the inout log file ", detail
            
    def parseStatLogs(self):
        logRec={}
        lineParsed=0
        lines=self.logPointer.readlines()
        while  lineParsed < len(lines):
            logRec={}
            #### read commit number
            while not (re.match("^[ ]*commit[ ]+[0-9a-fA-F]+$", lines[lineParsed],re.IGNORECASE)) and lineParsed< len(lines): # fine the frist commit message in file
                lineParsed=lineParsed+1
                print "warning expected commit doesn't show up" , lineParsed
            if  lineParsed< len(lines):   
                tokens=str(lines[lineParsed]).split(" ")
                #tokens.remove(' ') ## remove space if any
                logRec['commitNo']=str(tokens[1]).strip('\n')
                lineParsed=lineParsed+1
            ## read author info    
            if  lineParsed< len(lines):    
                if not (re.match("^[ ]*Author", lines[lineParsed],re.IGNORECASE)):
                    lineParsed=lineParsed+1
                    print "warning: expected author name doesn't show up"  , lineParsed
                    continue
                else:
                    tokens=str(lines[lineParsed]).split(":")
                    #tokens.remove(' ') ## remove space if any
                    tokens=tokens[1].split("<")
                    #tokens.remove(' ') ## remove space if any
                    logRec['authorName']=str(tokens[0]).strip()
                    logRec['authorEmail']=str(tokens[1]).strip('>\n')
                    lineParsed=lineParsed+1
            ### read date Data        
            if  lineParsed< len(lines):
                if not (re.match("^[ ]*Date", lines[lineParsed],re.IGNORECASE)):
                    lineParsed=lineParsed+1
                    print "warning expected Date  doesn't show up", lineParsed
                    lineParsed=lineParsed+1
                    continue
                else:
                    tokens=str(lines[lineParsed]).split("e:")
                    #tokens.remove(' ') ## remove space if any
                    logRec['date']=str(tokens[1]).strip()
                    lineParsed=lineParsed+1
            ##### read  Description
            if  lineParsed< len(lines):
                if not (lines[lineParsed]=='\n'):
                    lineParsed=lineParsed+1
                    print "warning expected commit description  doesn't show up", lineParsed
                    lineParsed=lineParsed+1
                    continue 
                else:
                    lineParsed=lineParsed+1
                    logRec['commitDesc']="";
                    tokens=str(lines[lineParsed]).replace('"','').split("]")
                    logRec['BugNo']=str(tokens[0]).strip('\n')+']'.replace('"','')
                    
                    while  not re.match(".*[|]+[ ]+[0-9]+[ ]+[+]*[-]*", lines[lineParsed], re.IGNORECASE) and lineParsed< len(lines):#not (lines[lineParsed]=='\n') and lineParsed<len(lines):
                        logRec['commitDesc']=str(logRec['commitDesc']).join(str(lines[lineParsed]).strip('\n').strip().replace('"',''))
                        lineParsed=lineParsed+1
                  
                    
            ### read diff detail data
            if lineParsed< len(lines):
                logRec['changeList']=[]
                change={}
                logRec['neighbors']=[]
                while not (re.match("^[ ]+[0-9]+[ ]+file[s]*[ ]+changed,", lines[lineParsed], re.IGNORECASE)) and lineParsed < len(lines):
                    tokens=str(lines[lineParsed]).split('|')
                    #filepath=(str(tokens[0]).split('/'))
                    pathtokens=(str(tokens[0])).split('/')
                    change['fileName']=str(pathtokens[len(pathtokens)-1]).strip()
                    logRec['neighbors'].append(str(pathtokens[len(pathtokens)-1]).strip())
                    #change['fileName']=(str(tokens[0]))
                    change['addedCount']=str(tokens[1]).count('+')
                    change['removedCount']=str(tokens[1]).count('-')
                    logRec['changeList'].append(change.copy())
                    lineParsed=lineParsed+1
            # read summery data        
            if lineParsed< len(lines):
                tokens=str(lines[lineParsed]).split(',')
                if re.match("[ ]*[0-9]+[ ]+insertion", tokens[1], re.IGNORECASE):
                    logRec['allInsertion']=(tokens[1].split(' ')[1])
                    if len(tokens)==3:
                        logRec['allRemove']=(tokens[2].split(' ')[1])
                    else:
                        logRec['allRemove']="0"
                else:
                    logRec['allRemove']=(tokens[1].split(' ')[1])
                    if len(tokens)==3:
                        logRec['allInsertion']=(tokens[2].split(' ')[1])
                    else:
                        logRec['allInsertion']="0"
                lineParsed=lineParsed+1
                self.logsData.append(logRec.copy())
                print logRec
                lineParsed=lineParsed+1          
                    
                                    
    def exportCVS(self,releaseNo,allChangesPath):
        allchange=open(allChangesPath,'w')
        allchange.write('releaseNo,fileName,filepath,commitNo,addedCount,removedCount,allInsertion,allRemove,authorName,authorEmail,commitDesc,date,BugNo,SVN-ID\n')
        for logRec in self.logsData:
            for change in logRec['changeList']:
                if re.match('.*[.]+java',change['fileName'],re.IGNORECASE):
                    allchange.write(releaseNo+','+change['fileName']+','+change['filePath']+','+logRec['commitNo']+','+str(change['addedCount'])+','+str(change['removedCount'])+',' 
                                    +str(logRec['allInsertion'])+','+str(logRec['allRemove'])+','+logRec['authorName']+','+logRec['authorEmail']+",'"+
                                     logRec['commitDesc']+"',"+logRec['date']+",'"+logRec['BugNo']+"',"+str(logRec['SVN-ID'])+"\n")
                   
        
        allchange.close()
        
    def calculateFileMetric(self):
        for commit in self.logsData:
            #neighbor=[]
            for change in commit['changeList']:
                if re.match('.*[.]+java',change['fileName'],re.IGNORECASE):
                    autorName=str([commit['authorName']])
                    if not change['filePath'] in self.filesMetric:
                        self.filesMetric[change['filePath']]={}
                        self.filesMetric[change['filePath']]['COMM']=0
                        self.filesMetric[change['filePath']]['authors']={}
                        self.filesMetric[change['filePath']]['ADD']=0
                        self.filesMetric[change['filePath']]['DEL']=0
                        self.filesMetric[change['filePath']]['neighbors']=[]
                        self.filesMetric[change['filePath']]['fileName']=change['fileName']
                    if not str([commit['authorName']]) in self.filesMetric[change['filePath']]['authors']:
                        self.filesMetric[change['filePath']]['authors'][autorName]={}
                        self.filesMetric[change['filePath']]['authors'][autorName]['ADD']=0
                        self.filesMetric[change['filePath']]['authors'][autorName]['DEL']=0 
                        self.filesMetric[change['filePath']]['authors'][autorName]['COMM']=0
                    
                    self.filesMetric[change['filePath']]['COMM']= self.filesMetric[change['filePath']]['COMM']+1
                    #if self.filesMetric[change['filePath']]['COMM']>1 :
                    #    pass
                    #if change['filePath']=='rt/rs/security/oauth-parent/oauth/src/main/java/org/apache/cxf/rs/security/oauth/client/OAuthClientUtils.java':
                    #   pass
                    
                    self.filesMetric[change['filePath']]['neighbors']=self.filesMetric[change['filePath']]['neighbors']+ commit['neighbors']
                    self.filesMetric[change['filePath']]['ADD']=self.filesMetric[change['filePath']]['ADD']+int(change['addedCount'])
                    self.filesMetric[change['filePath']]['DEL']=self.filesMetric[change['filePath']]['DEL']+int(change['removedCount'])
                    self.filesMetric[change['filePath']]['authors'][autorName]['COMM']=self.filesMetric[change['filePath']]['authors'][autorName]['COMM']+1
                    self.filesMetric[change['filePath']]['authors'][autorName]['ADD']=self.filesMetric[change['filePath']]['authors'][autorName]['ADD']+int(change['addedCount'])
                    self.filesMetric[change['filePath']]['authors'][autorName]['DEL']=self.filesMetric[change['filePath']]['authors'][autorName]['DEL']+int(change['removedCount'])
                    
                    if not autorName in self.devList:
                        self.devList[autorName]={}
                        self.devList[autorName]['EXP']=0
                        
                    self.devList[autorName]['EXP']=self.devList[autorName]['EXP'] + int(change['addedCount']) + int(change['removedCount'])
                    #self.releaseAllChanges=self.filesMetric[change['filePath']]['DEL']+self.filesMetric[change['filePath']]['ADD']+ self.releaseAllChanges
                else: 
                    #print  change['fileName']
                    pass

        sumall=0
        for file in self.filesMetric:
            sumall=sumall+ self.filesMetric[file]['ADD']+ self.filesMetric[file]['DEL']
        for tempauthor in self.devList:
            print tempauthor, self.devList[tempauthor]['EXP']
            self.releaseAllChanges=self.devList[tempauthor]['EXP']+self.releaseAllChanges
        print self.releaseAllChanges,sumall # release all change is sum of the exp for all author in release, for base release this two number should be same
        for file in self.filesMetric:
            if self.filesMetric[file]['COMM']>1:
                pass
                #print file
                #print self.filesMetric[file]['COMM']
                #print self.filesMetric[file]['neighbors']
        return self.filesMetric
        pass    
    def pasrLogs(self):
        if self.inputLogFormat=="--stat":
            self.parseStatLogs()
        elif self.inputLogFormat=="--numstat&shortstat":
            self.parseNumandShortStat()
            
            
            
    def findDevchange(self,authorsList,allchange):
        minorchange=0
        maxchange=0
        for authorName in authorsList:
            tempChange=authorsList[authorName]['ADD']+authorsList[authorName]['DEL']
            #if allchange
            if (tempChange/float(allchange))*100<=5:
                minorchange=minorchange+1
            if tempChange>maxchange:
                maxchange=tempChange
                ownerEXP=self.devList[authorName]['EXP']
                ownerName=authorName
        count=0
        sum1=1.0
        for authorName in authorsList:
            count=count+1
            sum1=sum1*self.devList[authorName]['EXP']
        if count>0:
            NonOwnerEXP=sum1**(1.0/count)
        else:
            NonOwnerEXP=0
        return [minorchange,maxchange,ownerEXP,NonOwnerEXP]

    def finDDEV(self,uptoReleasedata,releaseData,filePath):
        if uptoReleasedata==None:
            return (releaseData[filePath]['authors'])
        else:
            DDEVList=(releaseData[filePath]['authors']).copy()
            DDEV=len(DDEVList)
            if filePath in uptoReleasedata:
                for author in uptoReleasedata[filePath]['authors']:
                    if not author in DDEVList:
                        DDEVList[author]={}
                        DDEV=DDEV+1

            return  DDEVList                       
            
    def exportMetrics(self,uptoReleasedata,releaseData,exportPath):
            file=open(exportPath,"w")
            file.write('release,filePath,COMM,ADD,DEL,ADEV,DDEV,MINOR,OWN,SCTR,NCOMM,NADEV,NDDEV,NSCTR,OEXP,EXP,BUGY\n')
            for filePath in releaseData:
                COMM=releaseData[filePath]['COMM']
                ADD=releaseData[filePath]['ADD']
                DEL=releaseData[filePath]['DEL']
                if ADD+DEL==0:
                    print "warning in (number of change is Zero)" ,filePath 
                    continue
                ADEV=len(releaseData[filePath]['authors'])
                #DDEV=len(releaseData[filePath]['authors'])
                DDEV=len(self.finDDEV(uptoReleasedata, releaseData, filePath))
                Devchanges=self.findDevchange(releaseData[filePath]['authors'], ADD+DEL)
                SCTR=(ADD+DEL)/float(self.releaseAllChanges)
                MINOR=Devchanges[0]
                OWN=Devchanges[1]
                OEXP=Devchanges[2]
                EXP=Devchanges[3]
                NADEV=0
                NDDEV=0
                NCOMM=0
                NSCTR=0
                NADEVlist=[]
                NDDEVlist=[]
                for neighbor in releaseData[filePath]['neighbors']:
                    if not neighbor==filePath:
                        NCOMM=NCOMM+releaseData[neighbor]['COMM']
                        for author in releaseData[neighbor]['authors']:
                            if not author in NADEVlist:
                                NADEVlist.append(author)
                        
                        NADEV=NADEV+len(releaseData[neighbor]['authors'])
                        for author in self.finDDEV(uptoReleasedata, releaseData, neighbor):
                            if author not in NDDEVlist:
                                NDDEVlist.append(author)
                        NSCTR=NSCTR+((releaseData[neighbor]['ADD']+releaseData[neighbor]['DEL'])/float(self.releaseAllChanges))
                NADEV=len(NADEVlist) 
                NDDEV=len(NDDEVlist)       
                #if DDEV>ADEV+1:
                file.write("%s,%s,%d,%d,%d,%d,%d,%d,%d,%f,%d,%d,%d,%f,%d,%f,%s\n"%
                    (release,filePath,COMM, ADD,DEL,ADEV,DDEV,MINOR,OWN,round(SCTR,4),NCOMM,NADEV,NDDEV,round(NSCTR,4),OEXP,round(EXP,4),'N'))
                    #print filePath,COMM, ADD,DEL,ADEV,DDEV,MINOR,OWN,round(SCTR,4),NCOMM,NADEV,NDDEV,round(NSCTR,4),OEXP,round(EXP,4)
                    
            file.close()
            pass
       
    def parseNumandShortStat(self):  ### this function parses the logs generated by git log --shortstat --numstat --nomerges , create full json structure from commits 
        logRec={}
        lineParsed=0
        lines=self.logPointer.readlines()
        releaseChange=0
        while  lineParsed < len(lines):
            logRec={}
            #### read commit number
            while not (re.match("^[ ]*commit[ ]+[0-9a-fA-F]+$", lines[lineParsed],re.IGNORECASE)) and lineParsed< len(lines): # fine the frist commit message in file
                lineParsed=lineParsed+1
                print "warning expected commit doesn't show up" , lineParsed
            if  lineParsed< len(lines):   
                tokens=str(lines[lineParsed]).split(" ")
                #tokens.remove(' ') ## remove space if any
                logRec['commitNo']=str(tokens[1]).strip('\n')
                #print logRec['commitNo']
                lineParsed=lineParsed+1
            #### skip merge if there is any merge    
            if lineParsed< len(lines):
                if  re.match("^[ ]*Merge:", lines[lineParsed],re.IGNORECASE):
                    lineParsed=lineParsed+1
            ## read author info    
            if  lineParsed< len(lines):    
                if not (re.match("^[ ]*Author", lines[lineParsed],re.IGNORECASE)):
                    lineParsed=lineParsed+1
                    print "warning: expected author name doesn't show up"  , lineParsed
                    continue
                else:
                    tokens=str(lines[lineParsed]).split(":")
                    #tokens.remove(' ') ## remove space if any
                    tokens=tokens[1].split("<")
                    #tokens.remove(' ') ## remove space if any
                    logRec['authorName']=str(tokens[0]).strip()
                    logRec['authorEmail']=str(str(tokens[1]).strip('>\n'))
                    lineParsed=lineParsed+1
            ### read date Data        
            if  lineParsed< len(lines):
                if not (re.match("^[ ]*Date", lines[lineParsed],re.IGNORECASE)):
                    lineParsed=lineParsed+1
                    print "warning expected Date  doesn't show up", lineParsed
                    lineParsed=lineParsed+1
                    continue
                else:
                    tokens=str(lines[lineParsed]).split("e:")
                    #tokens.remove(' ') ## remove space if any
                    logRec['date']=str(tokens[1]).strip()
                    lineParsed=lineParsed+1
            ##### read  Description
            if  lineParsed< len(lines):
                if not (lines[lineParsed]=='\n'):
                    lineParsed=lineParsed+1
                    print "warning expected commit description  doesn't show up", lineParsed
                    lineParsed=lineParsed+1
                    continue 
                else:
                    lineParsed=lineParsed+1
                    logRec['commitDesc']="";
                    tokens=str(lines[lineParsed]).replace('"', '').split("]")
                    logRec['BugNo']=str(tokens[0]).strip('\n')+']'
                    logRec["SVN-ID"]=''
                    #while  not re.match("^[0-9]+[\t]+[0-9]+[\t]+.*[.]+.*", lines[lineParsed], re.IGNORECASE) and lineParsed< len(lines):#not (lines[lineParsed]=='\n') and lineParsed<len(lines):
                    while  not (re.match("^[0-9]+[\t]+[0-9]+[\t]+.*[.]+.*", lines[lineParsed], re.IGNORECASE) or  re.match("^[ ]*commit[ ]+[0-9a-fA-F]+$", lines[lineParsed],re.IGNORECASE)):
                        if re.match("[ ]+git-svn-id:",lines[lineParsed], re.IGNORECASE):
                            tempTokens=lines[lineParsed].split()
                            logRec["SVN-ID"]=tempTokens[len(tempTokens)-2]
                        #logRec['commitDesc']=str(logRec['commitDesc'])+(str(lines[lineParsed]).strip('\n').strip()).replace('"','')
                        lineParsed=lineParsed+1
                        if lineParsed>=len(lines):
                            break
            if lineParsed>=len(lines):
                            break
            #### some log doesn't include any change      
            if (re.match("^[ ]*commit[ ]+[0-9a-fA-F]+$", lines[lineParsed],re.IGNORECASE)):
                continue        
            ### read diff detail data
            if lineParsed< len(lines):
                logRec['changeList']=[]
                change={}
                logRec['neighbors']=[]
                while not (re.match("^[ ]+[0-9]+[ ]+file[s]*[ ]+changed,", lines[lineParsed], re.IGNORECASE)) and lineParsed < len(lines):
                    tokens=str(lines[lineParsed]).split()
                    #filepath=(str(tokens[0]).split('/'))
                    pathtokens=(str(tokens[2])).split('/')
                    change['fileName']=str(pathtokens[len(pathtokens)-1]).strip()
                    change['filePath']=str(tokens[2]).strip()
                    if re.match('.*[.]+java',str(tokens[2]).strip(),re.IGNORECASE):
                        logRec['neighbors'].append(str(tokens[2]).strip())
                    #change['fileName']=(str(tokens[0]))
                    change['addedCount']=str(tokens[0]).strip()
                    change['removedCount']=str(tokens[1]).strip()
                    logRec['changeList'].append(change.copy())
                    lineParsed=lineParsed+1
            # read summery data        
            if lineParsed< len(lines):
                tokens=str(lines[lineParsed]).split(',')
                if re.match("[ ]*[0-9]+[ ]+insertion", tokens[1], re.IGNORECASE):
                    logRec['allInsertion']=(tokens[1].split(' ')[1])
                    if len(tokens)==3:
                        logRec['allRemove']=(tokens[2].split(' ')[1])
                    else:
                        logRec['allRemove']="0"
                else:
                    logRec['allRemove']=(tokens[1].split(' ')[1])
                    if len(tokens)==3:
                        logRec['allInsertion']=(tokens[2].split(' ')[1])
                    else:
                        logRec['allInsertion']="0"
                lineParsed=lineParsed+1
                releaseChange=int(logRec['allRemove'])+int(logRec['allInsertion'])+releaseChange # all change, be carefull for java and non java file
                self.logsData.append(logRec.copy())
                
                lineParsed=lineParsed+1        

                    
            

    def exporrLog(self):
        pass


if __name__ == '__main__':
    ### the following script read up to release log and release log and calculate all process metric, then export it as CSV file
    total = len(sys.argv)
    if total<3:
        print "Eenter release number,UPto log log file geneated by --shortstat --numstat --no-merges , release  log file geneated by --shortstat --numstat --no-merges ,output filename "
        exit(2)
    else:
        gitParserrelease=gitParser()
        release=sys.argv[1]
        if sys.argv[2]==sys.argv[3]:
            basereleaseData=None
        else:
            gitParserrelease.logPath= sys.argv[2]
            gitParserrelease.inputLogFormat='--numstat&shortstat'
            gitParserrelease.loadLogs()
            gitParserrelease.pasrLogs()
            basereleaseData=gitParserrelease.calculateFileMetric()
            gitParserrelease.devList={}
            gitParserrelease.filesMetric={}
            gitParserrelease.logsData=[]
        
        gitParserrelease.logPath=sys.argv[3]
        outpufile=sys.argv[4]
        
        gitParserrelease.inputLogFormat='--numstat&shortstat'
        gitParserrelease.loadLogs()
        gitParserrelease.pasrLogs()
        currentRelease=gitParserrelease.calculateFileMetric()
        gitParserrelease.exportMetrics(basereleaseData, currentRelease, outpufile)

